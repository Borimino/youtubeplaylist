#!/bin/bash

for file in ???????????---*.mp3; do
	echo $file

	author_regex="---.*---(.*)---"
	[[ $file =~ $author_regex ]]
	tmp_author="${BASH_REMATCH[1]}"
	tmp_song_info=$(cat song_info.txt | grep "$tmp_author")
	tmp_song_name=$(echo $tmp_song_info | awk 'BEGIN { FS="¤" } { print $2 }')
	tmp_author2=$(echo $tmp_song_info | awk 'BEGIN { FS="¤" } { print $3 }')
	tmp_album=$(echo $tmp_song_info | awk 'BEGIN { FS="¤" } { print $4 }')
	#echo $tmp_author

	shown_song_name=""
	shown_author=""
	shown_album=""

	if [[ -n $tmp_song_name ]]; then
		[[ $file =~ $tmp_song_name ]]
		shown_song_name="${BASH_REMATCH[1]}"
	fi
	if [[ -n $tmp_author2 ]]; then
		[[ $file =~ $tmp_author2 ]]
		shown_author="${BASH_REMATCH[1]}"
	fi
	if [[ -n $tmp_album ]]; then
		[[ $file =~ $tmp_album ]]
		shown_album="${BASH_REMATCH[1]}"
	fi

	echo -n "$tmp_song_name "
	echo -n "$shown_song_name >"
	read -r song_name
	echo -n "$tmp_author2 "
	echo -n "$shown_author >"
	read -r author
	#echo -n "$tmp_album "
	#echo -n "$shown_author >"
	#read -r album
	album=$author

	if [[ -z $song_name ]]; then
		song_name=$tmp_song_name
		song_name2=$tmp_song_name
	else
		if [[ -z $tmp_song_name ]]; then
			#echo "$tmp_author¤$song_name" >> song_info.txt
			song_name2=$song_name
		else
			song_name2=$tmp_song_name
		fi
	fi

	if [[ -z $author ]]; then
		author=$tmp_author2
		author2=$tmp_author2
	else
		if [[ -z $tmp_author2 ]]; then
			#echo "$tmp_author¤$author" >> song_info.txt
			author2=$author
		else
			author2=$tmp_author2
		fi
	fi

	if [[ -z $album ]]; then
		album=$tmp_album
		album2=$tmp_album
	else
		if [[ -z $tmp_album ]]; then
			#echo "$tmp_author¤$album" >> song_info.txt
			album2=$album
		else
			album2=$tmp_album
		fi
	fi

	if [[ -z $tmp_song_name || -z $tmp_author2 || -z $tmp_album ]]; then
		#sed "/^$tmp_author¤.*/ p" song_info.txt
		sed -i "/^$tmp_author¤.*/ s/.*/$tmp_author¤$song_name2¤$author2¤$album2/" song_info.txt
	fi
	if [[ -z $tmp_song_name && -z $tmp_author2 && -z $tmp_album ]]; then
		echo "$tmp_author¤$song_name2¤$author2¤$album2" >> song_info.txt
	fi
	

	#echo "$song_name"
	#echo "$author"
	#echo "$album"
	./retag.sh "$song_name" "$author" "$album" "$file"

	printf '%0.1s' "-"{1..100}
	echo ""

	#song_name2=$(mid3v2 --list "$file" | grep "TIT2" | sed s/TIT2=//)
	#author2=$(mid3v2 --list "$file" | grep "TPE1" | sed s/TPE1=//)
	#album2=$(mid3v2 --list "$file" | grep "TALB" | sed s/TALB=//)

	#echo "$song_name2; $author2; $album2; $file"
done
