#!/bin/bash

for line in *.mp3; do
	songname=$(mid3v2 --list "$line" | grep "TIT2" | sed s/TIT2=//)
	author=$(mid3v2 --list "$line" | grep "TPE1" | sed s/TPE1=//)
	album=$(mid3v2 --list "$line" | grep "TALB" | sed s/TALB=//)

	printf "%-90s | %-20s | %-20s | %-20s\n" "$line" "$songname" "$author" "$album"
done
