#!/bin/bash

if [[ $# -ne 4 ]]; then
	echo "USAGE:
	$0 <Name regex> <Author regex> <Album regex> <Filename>
	"
	exit 1
fi

NAME_REGEX=$1

AUTHOR_REGEX=$2

ALBUM_REGEX=$3

FILENAME=$4

if [[ -n $NAME_REGEX ]]; then
	[[ $FILENAME =~ $NAME_REGEX ]]
	NAME="${BASH_REMATCH[1]}"
fi

if [[ -n $AUTHOR_REGEX ]]; then
	[[ $FILENAME =~ $AUTHOR_REGEX ]]
	AUTHOR="${BASH_REMATCH[1]}"
fi

if [[ -n $ALBUM_REGEX ]]; then
	[[ $FILENAME =~ $ALBUM_REGEX ]]
	ALBUM="${BASH_REMATCH[1]}"
fi

#echo "$NAME; $AUTHOR; $ALBUM; $FILENAME"
mid3v2 -t "$NAME" -a "$AUTHOR" -A "$ALBUM" "$FILENAME"

if [[ $FILENAME == ???????????---* ]]; then
	mv "$FILENAME" "${FILENAME#???????????---}"
	FILENAME=${FILENAME#???????????---}
fi

song_name2=$(mid3v2 --list "$FILENAME" | grep "TIT2" | sed s/TIT2=//)
author2=$(mid3v2 --list "$FILENAME" | grep "TPE1" | sed s/TPE1=//)
album2=$(mid3v2 --list "$FILENAME" | grep "TALB" | sed s/TALB=//)

echo "$song_name2; $author2; $album2"
