#!/bin/bash

MP4=".webm"

MP3=".mp3"

FILENAME=${1%$MP4}

ffmpeg -n -i "$FILENAME$MP4" -vn -f mp3 "$FILENAME$MP3" >/dev/null 2>&1
