#!/bin/bash

cd /home/markus/myscripts/youtubeplaylist

/usr/local/bin/youtube-dl -q --update
/usr/local/bin/youtube-dl -ci --quiet --no-warnings -o "%(id)s---%(title)s---%(uploader)s---.%(ext)s" https://www.youtube.com/playlist?list=LLEb0hC0ZhZ7dRTBhOOjVM6g

#ls ?????---* | while read line; do
for line in ???????????---*; do
	#echo $line
	newline2=${line#???????????---}
	# If the file already exists in a processed form, continue
	if [ -f "$newline2" ]; then
		continue
	fi
	if [[ $line == *.mp4 ]]; then
		newline2=${line#???????????---}
		newline2=${newline2%.mp4}.mp3
		# If the file already exists in a processed form, delete it
		if [ -f "$newline2" ]; then
			#rm "$line"
			continue
		fi
		./mp4tomp3.sh "$line"
		newline="${line%.mp4}.mp3"
		#echo "$line to $newline"
		./tags.sh "^(.*)\." "---.*---(.*)---" "---.*---(.*)---" "$newline"
	elif [[ $line == *.mkv ]]; then
		newline2=${line#???????????---}
		newline2=${newline2%.mkv}.mp3
		# If the file already exists in a processed form, delete it
		if [ -f "$newline2" ]; then
			#rm "$line"
			continue
		fi
		/home/markus/myscripts/youtubeplaylist/mkvtomp3.sh "$line"
		newline="${line%.mkv}.mp3"
		#echo "$line to $newline"
		/home/markus/myscripts/youtubeplaylist/tags.sh "^(.*)\." "---.*---(.*)---" "---.*---(.*)---" "$newline"
	elif [[ $line == *.webm ]]; then
		newline2=${line#???????????---}
		newline2=${newline2%.webm}.mp3
		# If the file already exists in a processed form, delete it
		if [ -f "$newline2" ]; then
			#rm "$line"
			continue
		fi
		/home/markus/myscripts/youtubeplaylist/webmtomp3.sh "$line"
		newline="${line%.webm}.mp3"
		#echo "$line to $newline"
		/home/markus/myscripts/youtubeplaylist/tags.sh "^(.*)\." "---.*---(.*)---" "---.*---(.*)---" "$newline"
	fi
	#/home/markus/myscripts/youtubeplaylist/tags.sh "^(.*)\." "---.*---(.*)---" "---.*---(.*)---" "$newline"
done 

cp *.mp3 /home/markus/Dropbox/music >/dev/null 2>&1

rm /home/markus/Dropbox/music/???????????---* >/dev/null 2>&1

ls ???????????---*.mp3 >/dev/null 2>&1 && printf "There are new videos\nRun ./retag_all.sh <song-name> <author> <album> <filename> to give them a propper name\nThe author and album should be correct\n"
