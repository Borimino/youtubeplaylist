#!/bin/bash

if [[ $# -ne 4 ]]; then
	echo "USAGE:
	$0 <Name regex> <Author regex> <Album regex> <Filename>
	"
	exit 1
fi

NAME_REGEX=$1

AUTHOR_REGEX=$2

ALBUM_REGEX=$3

FILENAME=$4

[[ $FILENAME =~ $NAME_REGEX ]]
NAME="${BASH_REMATCH[1]}"

[[ $FILENAME =~ $AUTHOR_REGEX ]]
AUTHOR="${BASH_REMATCH[1]}"

[[ $FILENAME =~ $ALBUM_REGEX ]]
ALBUM="${BASH_REMATCH[1]}"

#echo "$NAME; $AUTHOR; $ALBUM; $FILENAME"
mid3v2 -t "$NAME" -a "$AUTHOR" -A "$ALBUM" "$FILENAME"

